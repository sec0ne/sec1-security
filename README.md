# Sec1 Security

## Usage

This component integrates [Sec1](https://sec1.io/) to conduct vulnerability scans on your Gitlab projects. Sec1 is a powerful tool that helps identify security vulnerabilities within your codebase.


```yaml
include:
  - component: gitlab.com/sec0ne/sec1-security/sec1-security@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

This will add a `sec1-security` job to the pipeline.

The template should work without modifications but you can customize the template settings if
needed: 

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `test`      | The stage where you want the job to be added |

### Variables

| Variable | Description |
| -------- | ----------- |
| `SEC1_API_KEY` | Required. You Sec1 Api Key |
| `SEC1_SCAN_THRESHOLD` | OPtional. Sec1 scan supports setting up threshold values. If the scan reports vulnerabilities exceeding the specified thresholds, Sec1 Security will mark the build as failed. You can set threshold values for different severities such as critical, high, medium, and low. |


## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components
